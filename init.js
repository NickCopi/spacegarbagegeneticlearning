
window.addEventListener('load',()=>{
	let games = [];
	let generation = 1;
	let maxScore = 0;
	let info = document.getElementById('info');
	let generationText = 'Generation 1; ';
	for(let i = 0; i < 16;i++){
		let canvas = document.createElement('canvas');
		document.body.appendChild(canvas);
		canvas.setAttribute('index',i);
		games.push(new Game(canvas, new AI()));
		canvas.addEventListener('mouseover',function(){
			info.innerText = generationText + games[this.getAttribute('index')].scene.ai.dna.toString();
		});
	}
	setInterval(()=>{
		for(let i = 0; i < games.length; i++){
			if(!games[i].scene.gameEnd) return;
		}
		games.sort((a,b)=>b.scene.score - a.scene.score);
		const masterDNA = games[0].scene.ai.dna;
		const secondDNA = games[1].scene.ai.dna;
		if(games[0].scene.score > maxScore)
			maxScore = games[0].scene.score;
		games = games.map(game=>{
			clearInterval(game.scene.interval);
			const random = Math.floor(Math.random()*100);
			let dna;
			if(random < 85)
				dna = masterDNA;
			else if(random < 95) 
				dna = secondDNA;
			else
				dna = new DNA();
			const newDNA = Object.assign(new DNA, dna);
			newDNA.mutate();
			return new Game(game.scene.canvas, new AI(newDNA));
		});
		generation++;
		generationText = 'Generation ' + generation + '; Best Score ' + maxScore + '; '; 

	},1000)

});

class AI{
	constructor(dna){
		if(!dna) this.dna = new DNA();
		else this.dna = dna;
		this.targetGun = 0;
	}
	setScene(scene){
		//console.log(this.dna);
		this.scene = scene;
	}
	update(){
		this.scene.keys = [];
		this.scene.you.shooting = true;
		this.doMovement();
		this.checkGun();
	}

	checkGun(){
		if(!(this.scene.time % this.dna.swapInterval)){
			this.swapGun();
		}
		if(this.targetGun !== this.scene.you.currentWeapon){
			let you = this.scene.you;
			you.currentWeapon--;
			if(you.currentWeapon < 0) you.currentWeapon = you.weapons.length-1;
		}
	}
	swapGun(){
		const preferenceSet = this.dna.gunPreferences.slice(0,this.scene.you.weapons.length);
		const max = preferenceSet.reduce((sum,next)=>sum+next);
		const score = Math.random()*max	
		let sum = 0;
		let i;
		for(i = 0; i < preferenceSet.length; i++){
			sum += preferenceSet[i];	
			if(sum > score) break;
		}
		if(i !== this.scene.you.currentWeapon)
			this.targetGun = i;

	}

	moveDefault(){
		this.moveTo(this.dna.defaultY,this.dna.defaultAngle);
	}

	moveTo(y,angle){

		if(Math.random() > this.dna.rotateWeight){
			if(this.scene.you.getFirePoint().y > y)
				this.moveUp();
			else
				this.moveDown();
		} else {
			if(this.scene.you.theta > angle)
				this.rotateLeft();
			else 
				this.rotateRight();
		}
	}


	moveUp(){
		this.scene.keys[87] = true;
	}
	moveDown(){
		this.scene.keys[83] = true;
	}
	rotateRight(){
		this.scene.keys[68] = true;
	}
	rotateLeft(){
		this.scene.keys[65] = true;
	}
	doMovement(){
		const target = this.getTarget();
		if(!target) return this.moveDefault();
		const aimPoint = this.getAimPoint(target);
		const angle = this.getAngle(aimPoint);
		this.moveTo(aimPoint.y,angle);
		
	}
	getAngle(aimPoint){
		const opposite = aimPoint.y-this.scene.you.getCenter().y;
		const adjacent = aimPoint.x-this.scene.you.getCenter().x;
		return Math.atan(opposite/adjacent);
	}
	getAimPoint(target){
		const distance = this.distance(this.scene.you.getFirePoint(),target);
		const time = distance/this.scene.you.weapons[this.scene.you.currentWeapon].speed;
		const targetMoved = time * target.speed;
		const targetX = target.x - (targetMoved + this.dna.aimAhead);

		return {
			x: targetX,
			y: target.y + target.height * this.dna.heightPercent
		}
	}
	distance(a,b){
		return Math.sqrt(Math.pow(b.x - a.x,2) + Math.pow(b.y - a.y,2));
	}
	getTarget(){
		const targets = this.getTargets().sort((a,b)=>a.time-b.time);
		let target = targets.find(target=>target.giveUpDistance < target.time);
		if(!target) target = targets[0];
		return target;
	}
	getTargets(){
		return this.scene.enemies.map(enemy=>{
			return {
				x: enemy.x,
				y: enemy.y,
				height: enemy.height,
				time: enemy.x/enemy.speed,
				speed: enemy.speed,
				health: enemy.lines.length
			}
		});
	}
	getGuns(){
		
	}
}

class DNA{
	constructor(){
		//time to wait before swapping weapons
		this.swapInterval = Math.floor(Math.random()*100);
		//time at which shooting enemies is worthless
		this.giveUpDistance = Math.floor(Math.random()*500);
		//aim ahead, how much x to aim ahead of an enemy, somehow factors with angle and enemy speed
		this.aimAhead = Math.floor(Math.random()*100);
		//default angle to aim for
		this.defaultAngle = Math.floor(Math.random()*Math.PI) - Math.PI/2;
		//default y to aim for
		this.defaultY = Math.floor(Math.random() * (720-40));
		//weight to rotate instead of move
		this.rotateWeight = Math.random();
		//percent of height to aim for of a target
		this.heightPercent = Math.random();
		//gun preference weights
		this.gunPreferences = [];
		for(let i = 0; i < 9; i++){
			this.gunPreferences.push(Math.floor(Math.random()*100));
		}
		
	}
	mutate(){
		this.swapInterval *= this.getMutation();
		this.giveUpDistance *= this.getMutation();
		this.aimAhead *= this.getMutation();
		this.defaultAngle += (Math.random()*(Math.PI/20)) - (Math.PI/40);
		this.defaultY *= this.getMutation();
		this.rotateWeight *= this.getMutation();
		this.heightPreference *= this.getMutation();
		this.gunPreferences = this.gunPreferences.map(pref=>pref*this.getMutation());
	}
	getMutation(){
		return 1 + ((Math.floor(Math.random()*10)+1)/100 - 0.05);
	}
	toString(){
		return `Weapon swap frame wait: ${this.swapInterval}; Enemy frames left on screen to give up at ${this.giveUpDistance}; Pixels to aim ahead of an enemies calculated trajectory ${this.aimAhead}; Angle to aim at when no enemies are around ${this.aimAhead}; Y position to move to when no enimes are around ${this.defaultY}; Weight towards rotating instead of moving ${this.rotateWeight}; Vertical percent of height of enemy to aim at ${this.heightPercent}; Weapon preference weights ${this.gunPreferences.toString()}`
	}

}
